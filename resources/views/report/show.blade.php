@extends('layout.main')
@section('content')


    <!-- ***** Breadcrumb Area Start ***** -->
    <div class="breadcrumb-area">
        <div class="container h-100">
            <div class="row h-100 align-items-end">
                <div class="col-10">
                    <div class="breadcumb--con">
                        <h2 class="title">รายงาน</h2>

                    </div>
                </div>

            </div>
        </div>
        <!-- Background Curve -->
        <div class="breadcrumb-bg-curve">
            <img src="./img/core-img/curve-52.png" alt="">
        </div>
    </div>
    <!-- ***** Breadcrumb Area End ***** -->

    <!-- ***** Contact Area Start ***** -->
    <section class="uza-contact-area section-padding-80" style="margin-top:-10%;">

        <div class="container">
            {!! Form::open(['url' => secure_url('export')]) !!}
            <div class="row">
                <div class="col-md-3">
                    <p style="font-size: 1.8em;text-align: right;"><strong>ประเภทรายงาน* :</strong></p>
                </div>
                <div class="col-md-5">
                    {!! Form::select('type', [
    'all' => 'ทั้งหมด',
    'YY' => 'ผู้สมัครที่น่าสนใจ',
    'Y' => 'ผู้สมัครไม่น่าสนใจ',
    'N' => 'ผู้สมัครที่ยังไม่ได้ตัดสินใจ'],null,['class'=>'form-control','placeholder'=>'กรุณาเลือกประเภทรายงาน']); !!}

                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <p style="font-size: 1.8em;text-align: right;">วันที่เริ่ม* :</p>
                </div>
                <div class="col-md-5">

                    <input type="date" class="form-control" name="date_start" id="date_start">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <p style="font-size: 1.8em;text-align: right;">วันที่สิ้นสุด* :</p>
                </div>
                <div class="col-md-5">
                    <input class="form-control" name="date_end" id="date_end" type="date">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">

                </div>
                <div class="col-md-2">
                    <button class="btn btn-outline-success" id="export">Export</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection
