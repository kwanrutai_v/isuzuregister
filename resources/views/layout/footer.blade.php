
<!-- ***** Footer Area Start ***** -->
<footer class="footer-area section-padding-80-0">
    <div class="container">
        <div class="row justify-content-between">

            <!-- Single Footer Widget -->
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="single-footer-widget mb-80">
                    <!-- Widget Title -->
                    <h4 class="widget-title">ข้อมูลติดต่อเจ้าหน้าที่</h4>

                    <!-- Footer Content -->
                    <div class="footer-content mb-15">
                        <h3>076-355112 ต่อ 809</h3>
                        <p>99-99/1 ม.4 ถ.เทพกระษัตรี <br>
                            ต.เกาะแก้ว อ.เมือง จ.ภูเก็ต 83000<br> hr@isuzuandamansales.com</p>
                    </div>

                </div>
            </div>

            <!-- Single Footer Widget -->
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="single-footer-widget mb-80">
                    <!-- Widget Title -->
                    <h4 class="widget-title">เว็บไซต์สมัครงาน</h4>

                    <!-- Nav -->
                    <nav>
                        <ul class="our-link">
                            <li><a href="www.isuzuandamansales.com/isuzu_register/position.php">ตำแหน่งงานว่าง</a></li>
                            <li><a href="www.isuzuandamansales.com/isuzu_register/regis.php">สมัครงาน</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <!-- Single Footer Widget -->
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="single-footer-widget mb-80">
                    <!-- Widget Title -->
                    <h4 class="widget-title">เว็บไซต์บริษัท</h4>

                    <!-- Nav -->
                    <nav>
                        <ul class="our-link">
                            <li><a href="www.isuzuandamansales.com">isuzuandamansales</a></li>
                            <li><a href="www.isuzuandamansales.com/isuzu-service">เว็บไซต์ฝ่ายศูนย์บริการหลังการขาย</a>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>

            <!-- Single Footer Widget -->
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="single-footer-widget mb-80">
                    <!-- Widget Title -->
                    <h4 class="widget-title">สื่อออนไลน์</h4>

                    <div class="footer-social-info">
                        <a href="https://www.facebook.com/isuzuandamanfunclub/" class="facebook" data-toggle="tooltip"
                           data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/FunIsuzu" class="twitter" data-toggle="tooltip"
                           data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.instagram.com/isuzuandaman" class="instagram" data-toggle="tooltip"
                           data-placement="top" title="Instagram"><i class="fa fa-instagram"></i></a>
                        <a href="#" class="youtube" data-toggle="tooltip" data-placement="top" title="YouTube"><i
                                class="fa fa-youtube-play"></i></a>
                    </div>
                </div>
            </div>

        </div>

        <div class="row" style="margin-bottom: 30px;">

            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script>
            Isuzuandamansales.com
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </div>

    </div>
</footer>
