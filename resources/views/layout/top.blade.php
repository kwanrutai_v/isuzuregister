<!-- Preloader -->
<div id="preloader">
    <div class="wrapper">
        <div class="cssload-loader"></div>
    </div>
</div>

<!-- ***** Top Search Area Start ***** -->
<div class="top-search-area">
    <!-- Search Modal -->
    <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <!-- Close Button -->
                    <button type="button" class="btn close-btn" data-dismiss="modal"><i class="fa fa-times"></i>
                    </button>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- ***** Top Search Area End ***** -->

<!-- ***** Header Area Start ***** -->
<header class="header-area">
    <!-- Main Header Start -->
    <div class="main-header-area">
        <div class="classy-nav-container breakpoint-off">
            <!-- Classy Menu -->
            <nav class="classy-navbar justify-content-between" id="uzaNav">

                <!-- Logo -->
                <a class="nav-brand" href="https://www.isuzuandamansales.com/isuzu_register/"><img src="{{ asset('img/core-img/logo1.png') }}" alt=""></a>

                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <!-- Menu -->
                <div class="classy-menu">
                    <!-- Menu Close Button -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>

                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul id="nav">
                            <li><a href="https://www.isuzuandamansales.com/isuzu_register/in_admin.php">หน้าหลัก</a></li>
                            <li class="current-item"><a href="{{ url('report') }}">รายงาน</a></li>
                            <li><a href="https://www.isuzuandamansales.com/isuzu_register/add_position.php">เพิ่มตำแหน่งงานว่าง</a></li>
                            <li><a href="https://www.isuzuandamansales.com/isuzu_register">หน้าสมัครงาน</a></li>
                            <li><a href="https://www.isuzuandamansales.com/isuzu_register/profile.php">User</a></li>
                        </ul>

                        <!-- Get A Quote -->
                        <div class="get-a-quote ml-4 mr-3">
                            <a class="btn uza-btn" href="https://www.isuzuandamansales.com/isuzu_register/logout.php"
                               style="font-size:1.5em; font-family:'psl244pro';width:auto;">Logout</a>
                        </div>

                    </div>
                    <!-- Nav End -->

                </div>
            </nav>
        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->
