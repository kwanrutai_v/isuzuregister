<!-- ******* All JS Files ******* -->
<!-- jQuery js -->
<script src="{{ secure_asset('js/jquery.min.js') }}"></script>
<!-- Popper js -->
<script src="{{ secure_asset('js/popper.min.js') }}"></script>
<!-- Bootstrap js -->
<script src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
<!-- All js -->
<script src="{{ secure_asset('js/uza.bundle.js') }}"></script>
<!-- Active js -->
<script src="{{ secure_asset('js/default-assets/active.js') }}"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
