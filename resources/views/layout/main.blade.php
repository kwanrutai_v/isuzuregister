<!DOCTYPE html>
<html lang="en">

<head>
    @include('layout.header')
</head>

<body>
<!-- Preloader -->
@include('layout.top')

<!-- ***** Breadcrumb Area Start ***** -->
@yield('content')
<!-- ***** Contact Area End ***** -->


<!-- ***** Footer Area Start ***** -->
@include('layout.footer')
<!-- ***** Footer Area End ***** -->

<!-- ******* All JS Files ******* -->
<!-- jQuery js -->
@include('layout.ja')
@yield('script')
</body>

</html>
