<?php

namespace App\Exports;

use App\Http\Controllers\ExportController;
use App\Models\Regis;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportExcel implements FromCollection,WithHeadings
{



    public function headings(): array
    {
        return [
            'วันที่สมัคร',
            'ชื่อ - นามสกุล',
            'ตำแหน่งที่สมัคร 1',
            'ตำแหน่งที่สมัคร 2',
            'สาขาที่สมัคร 1',
            'สาขาที่สมัคร 2',
            'อายุ',
            'วุฒิการศึกษา',
            'ทักษะการขับรถยนต์',
            'เบอร์โทร',
            'ราชการทหาร',
            'ช่องทางการรับสมัคร',
            'เหคุผลที่ไม่เลือก'
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return collect(ExportController::getReport1());
//        return Regis::all();
    }
}
