<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Regis extends Model
{
    use HasFactory;

    protected $table = 'regis';
    protected $fillable = [
        'interest_work',
        'interest_work1',
        'branch1',
        'branch2',
        'startday',
        'money',
        'news',
        'title_name',
        'firstname_th',
        'lastname_th',
        'nickname_th',
        'title_nameen',
        'firstname_en',
        'lastname_en',
        'nickname_en',
        'dtp_input2',
        'age',
        'weight', 'height', 'sex', 'military',
        'blood', 'nationality', 'race', 'religion', 'facebook',
        'line', 'ID_card', 'dateday', 'phone', 'email', 'disease',
        'disease_txt', 'criminal', 'criminal_txt', 'surgery', 'surgery_txt',
        'disabled','disabled_txt','no','moo','soi','road','selProvince',
        'selAmphur','selTumbon','zipcode','selProvince1',
        'selAmphur1','selTumbon1','zipcode1','selProvince_domicile',
        'selAmphur_domicile','selTumbon_domicile','zipcode_domicile',
        'educational','university','education','subjects','year','GPA','marital_status',
        'title_namemarital','firstname_marital','lastname_marital','career_marital',
        'children','phone_marital','phone_maritaloffice','addressoffice_marital','address_marital',
        'title_namefather','firstname_father','lastname_father','status_father','age_father',
        'career_father','posiotion_father','phone_father','addressoffice_father','address_father',
        'title_namemother','firstname_mother','lastname_mother','status_mother',
        'motorcycles','car','date_regis','addmore','interest_comment'
    ];
}
