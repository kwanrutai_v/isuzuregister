<?php

namespace App\Http\Controllers;

use App\Exports\ReportExcel;
use App\Models\Regis;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;

class ExportController extends Controller
{
    public static function getReport1()
    {

        $type = \request()->type;
        if ($type == "all") {
            $data = Regis::whereBetween('date_regis',[\request()->date_start,\request()->date_end])
                ->where('firstname_th','<>','')
                ->get();
        } else {
            $data = Regis::where('interest', '=', $type)
                ->whereBetween('date_regis',[\request()->date_start,\request()->date_end])
                ->where('firstname_th','<>','')
                ->get();
        }


//        $data = Regis::all();
        $i = 0;
        $count = 0;
        if ($data->count() > 0) {

            foreach ($data as $data) {
                $i++;

                $recode[] = array(

                    'วันที่สมัคร' => $data->date_regis,
                    'ชื่อ - นามสกุล' => $data->title_name . $data->firstname_th . " " . $data->lastname_th,
                    'ตำแหน่งที่สมัคร 1' => $data->interest_work,
                    'ตำแหน่งที่สมัคร 2' => $data->interest_work1,
                    'สาขาที่สมัคร 1' => $data->branch1,
                    'สาขาที่สมัคร 2' => $data->branch2,
                    'อายุ' => $data->age,
                    'วุฒิการศึกษา' => $data->education,
                    'ทักษะการขับรถยนต์' => $data->car,
                    'เบอร์โทร' => $data->phone,
                    'ราชการทหาร' => $data->military,
                    'ช่องทางการรับสมัคร' => $data->news,
                    'เหตุผลที่ไม่เลือก' => $data->interest_comment
                );
            }
        } else {
            $recode[] = array(

                'วันที่สมัคร' => '',
                'ชื่อ - นามสกุล' => '',
                'ตำแหน่งที่สมัคร 1' => '',
                'ตำแหน่งที่สมัคร 2' => '',
                'สาขาที่สมัคร 1' => '',
                'สาขาที่สมัคร 2' => '',
                'อายุ' => '',
                'วุฒิการศึกษา' => '',
                'ทักษะการขับรถยนต์' => '',
                'เบอร์โทร' => '',
                'ราชการทหาร' => '',
                'ช่องทางการรับสมัคร' => '',
                'เหตุผลที่ไม่เลือก' => ''
            );
        }
        return $recode;

    }

    public function export(Excel $excel,ReportExcel $reportExcel){
        return $excel->download($reportExcel,'Report-register-'.Carbon::now()->format('Y-m-d').'.xlsx');
    }
}
